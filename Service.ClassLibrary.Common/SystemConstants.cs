﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ClassLibrary.Common
{
    [Serializable()]
    public class ActionResult
    {
        public const string ACTION_FAILED = "Failed";
        public const string EXCEPTION_ENCOUNTERED = "Exception";
        public const string ACTION_SUCCESSFUL = "Success";
    }

    [Serializable()]
    public class Output
    {
        public const int FAILED = 0;
        public const int SUCCESSFUL = 1;
    }


    public class MessageAlert
    {

        public const string NO_REPONSE = "API request did not receive a response";
        public const string EMPTY_REPONSE = "API request received an empty response";
        public const string SAVE_DEPOSIT_SUCCESS = "Successfully saved deposit data";
        public const string SAVE_DEPOSIT_FAILED = "Save deposit data failed";
        public const string SAVE_BAG_REPLACEMENT_SUCCESS = "Successfully saved bag replacement data";
        public const string SAVE_BAG_REPLACEMENT_FAILED = "Save bag replacment data failed";

    }
}
