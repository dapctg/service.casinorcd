﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Service.ClassLibrary.Common
{
    public class AppConfig
    {
        private static readonly AppConfig instance = new AppConfig();
        private AppConfig() { InitModel(this); }
        public static AppConfig GetSingleton() { return instance; }

        #region configs
        
        public string RCDConnectionString { get; set; }
        public string DepositImportPath { get; set; }
        public string BagReplacementImportPath { get; set; }
        public string DiagnosticsImportPath { get; set; }
        
        public string BakPath { get; set; }
        public string ErrorPath { get; set; }
        public string FileFilter { get; set; }
        public bool IsSchedule { get; set; }
        public string FTPProtocol { get; set; }
        public string FTPHostName { get; set; }
        public string FTPUserName { get; set; }
        public string FTPPassword { get; set; }
        public string FTPPortNumber { get; set; }
        public string FTPImportPath { get; set; }
        public string FTPExportPath { get; set; }
        public string FTPSshHostKeyFingerprint { get; set; }
        public string SendFrom { get; set; }
        public string EmailPassword { get; set; }
        public string SMTPHost { get; set; }
        public string SMTPPort { get; set; }
        public string EmailReceipients { get; set; }
        public int SendingEmailCount { get; set; }
        public int SendingEmailMinutesLimit { get; set; }
        public int TimerMinute { get; set; }
        
        #endregion

        #region utils
        private string GetValue(string key)
        {

            if (System.Configuration.ConfigurationManager.AppSettings[key] != null)
            {
                return System.Configuration.ConfigurationManager.AppSettings[key];
            }
            else
            {
                return string.Empty;
            }
        }
        #region InitModelssss

        private void InitModel(object model)
        {
            PropertyInfo[] pros = model.GetType().GetProperties();
            foreach (PropertyInfo pi in pros)
            {
                if (pi.CanWrite)
                {
                    try
                    {
                        pi.SetValue(model, Convert.ChangeType(GetValue(pi.Name), pi.PropertyType), null);
                    }
                    catch (System.Exception ex)
                    {
                        
                    }
                }
            }
        }
        #endregion
        #endregion

    }
}

