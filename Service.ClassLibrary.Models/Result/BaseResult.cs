﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ClassLibrary.Models.Result
{
    public class BaseResult
    {
        public string Result { get; set; }
        public string Message { get; set; }
        public string ReturnCode { get; set; }
        public string ErrorMessage { get; set; }
        public Exception ObjectException { get; set; }
    }
}
