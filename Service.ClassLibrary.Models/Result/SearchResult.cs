﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ClassLibrary.Models.Result
{
    public class SearchResult: BaseResult
    {
        public DataTable DataTable { get; set; }
    }
}
