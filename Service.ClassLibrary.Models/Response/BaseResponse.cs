﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ClassLibrary.Models.Response
{
    public class BaseResponse
    {
        public string Response { get; set; }
        public string Message { get; set; }
        public string ReturnCode { get; set; }
        public string ErrorMessage { get; set; }
        public Exception ObjectException { get; set; }
    }
}
