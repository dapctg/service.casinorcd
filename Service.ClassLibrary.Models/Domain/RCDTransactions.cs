﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ClassLibrary.Models.Domain
{
    public class RCDTransactions
    {
        public string DepositID { get; set; }
        public string DepositDateTime { get; set; }
        public string User { get; set; }
        public string MachineId { get; set; }
        public string Box { get; set; }
        public string DepositData { get; set; }
        public int Result { get; set; }
        public string ResultMessage { get; set; }

    }
}
