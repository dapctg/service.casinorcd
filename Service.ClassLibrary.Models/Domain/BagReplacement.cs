﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ClassLibrary.Models.Domain
{
    public class BagReplacement
    {
        public string Time { get; set; }
        public string User { get; set; }
        public string MachineID { get; set; }
        public string OldBox { get; set; }
        public string NewBox { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
