﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Service.ClassLibrary.Models.Domain
{
    
    //public class DepositDetails
    //{
    
    //    public string Currency { get; set; }
    //    public string Denomination { get; set; }
    //    public string Qty { get; set; }
    //}


    //public class Deposit
    //{
    //    public List<DepositDetails> DepositDetails { get; set; }
    //}


    public class DepositData
    {
        public string DepositID { get; set; }
        public string DepositDateTime { get; set; }
        public string User { get; set; }
        public string MachineID { get; set; }
        public string Box { get; set; }
        public string DepositDetails { get; set; }
        
    }

}
