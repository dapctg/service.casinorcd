﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ClassLibrary.DataAccessLayer.DBProvider
{

    public class Parameters
    {
        public const string DEPOSIT_ID = "@DepositID";
        public const string DEPOSIT_DATETIME = "@DepositDateTime";
        public const string REPLACEMENT_DATETIME = "@ReplacementDateTime";
        public const string START_TIME = "@StartTime";
        public const string END_TIME = "@EndTime";
        public const string USER = "@User";
        public const string MACHINE_ID = "@MachineId";
        public const string BOX = "@Box";
        public const string OLD_BOX = "@OldBox";
        public const string NEW_BOX = "@NewBox";
        public const string DEPOSIT_DATA = "@DepositData";
        public const string RESULT = "@Result";
        public const string RESSULT_MESSAGE = "@ResultMessage";

    }
}