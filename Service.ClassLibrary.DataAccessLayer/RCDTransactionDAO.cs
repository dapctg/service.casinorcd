﻿
using Service.ClassLibrary.DataAccessLayer.DBProvider;
using System.Data;
using Service.ClassLibrary.Models.Domain;
using System.Data.SqlClient;

namespace Service.ClassLibrary.DataAccessLayer
{
    public class RCDTransactionDAO
    {
        public RCDTransactionDAO()
        { }

        public void SaveDepositData(DepositData depositData, out int outResult, out string outResultMessage)
        {
            ConnectionValues ConnStr = new ConnectionValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.RCDConn());

            SqlParameter[] @params = new SqlParameter[8];

            @params[0] = new SqlParameter(Parameters.DEPOSIT_ID, SqlDbType.NVarChar);
            @params[0].Value = depositData.DepositID;

            @params[1] = new SqlParameter(Parameters.DEPOSIT_DATETIME, SqlDbType.VarChar);
            @params[1].Value = depositData.DepositDateTime;

            @params[2] = new SqlParameter(Parameters.USER, SqlDbType.NVarChar);
            @params[2].Value = depositData.User;

            @params[3] = new SqlParameter(Parameters.MACHINE_ID, SqlDbType.NVarChar);
            @params[3].Value = depositData.MachineID;

            @params[4] = new SqlParameter(Parameters.BOX, SqlDbType.NVarChar);
            @params[4].Value = depositData.Box;

            @params[5] = new SqlParameter(Parameters.DEPOSIT_DATA, SqlDbType.NVarChar);
            @params[5].Value = depositData.DepositDetails;

            @params[6] = new SqlParameter(Parameters.RESULT, SqlDbType.Int);
            @params[6].Direction = ParameterDirection.Output;

            @params[7] = new SqlParameter(Parameters.RESSULT_MESSAGE, SqlDbType.VarChar, 255);
            @params[7].Direction = ParameterDirection.Output;

            try
            {
                sqldb.ExecuteReader("SaveDepositData", CommandType.StoredProcedure, @params);

                outResult = int.Parse(@params[6].Value.ToString());
                outResultMessage = @params[7].Value.ToString();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }


        public void SaveBagReplacement(BagReplacement bagReplacement, out int outResult)
        {
            ConnectionValues ConnStr = new ConnectionValues();
            SqlDatabase sqldb = new SqlDatabase(ConnStr.RCDConn());

            SqlParameter[] @params = new SqlParameter[8];

            @params[0] = new SqlParameter(Parameters.REPLACEMENT_DATETIME, SqlDbType.VarChar);
            @params[0].Value = bagReplacement.Time;

            @params[1] = new SqlParameter(Parameters.START_TIME, SqlDbType.VarChar);
            @params[1].Value = bagReplacement.StartTime;

            @params[2] = new SqlParameter(Parameters.END_TIME, SqlDbType.VarChar);
            @params[2].Value = bagReplacement.EndTime;

            @params[3] = new SqlParameter(Parameters.OLD_BOX, SqlDbType.NVarChar);
            @params[3].Value = bagReplacement.OldBox;

            @params[4] = new SqlParameter(Parameters.NEW_BOX, SqlDbType.NVarChar);
            @params[4].Value = bagReplacement.NewBox;

            @params[5] = new SqlParameter(Parameters.USER, SqlDbType.NVarChar);
            @params[5].Value = bagReplacement.User;

            @params[6] = new SqlParameter(Parameters.MACHINE_ID, SqlDbType.NVarChar);
            @params[6].Value = bagReplacement.MachineID;

            @params[7] = new SqlParameter(Parameters.RESULT, SqlDbType.Int);
            @params[7].Direction = ParameterDirection.Output;
            

            try
            {
                sqldb.ExecuteReader("SaveBagReplacement", CommandType.StoredProcedure, @params);

                outResult = int.Parse(@params[7].Value.ToString());
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}

