﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.ClassLibrary.DataAccessLayer
{
    public class ConnectionValues
    {

        public string conn()
        {

            string connstring = null;

            connstring = ConfigurationManager.ConnectionStrings["ODBCConnectionString"].ConnectionString;
            return connstring;

        }


        public string MySQLConn()
        {

            string connString = null;

            connString = ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString;
            return connString;

        }

        public string RCDConn()
        {

            string connString = null;

            connString = ConfigurationManager.ConnectionStrings["RCDConnectionString"].ConnectionString;
            return connString;

        }
    }
}
