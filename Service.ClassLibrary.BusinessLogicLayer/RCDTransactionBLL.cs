﻿using Service.ClassLibrary.Models.Domain;
using Service.ClassLibrary.Models.Result;
using Service.ClassLibrary.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service.ClassLibrary.Common;

namespace Service.ClassLibrary.BusinessLogicLayer
{
    
    public class RCDTransactionBLL
    {
        RCDTransactionDAO dao = new RCDTransactionDAO();
        public RCDTransactionBLL()
        { }

        public AddRecordResult SaveDepositData(DepositData depositData)
        {
            AddRecordResult result = new AddRecordResult();

            try
            {
                dao.SaveDepositData(depositData, out int outResult, out string outResultMessage);

                if (outResult == Output.SUCCESSFUL)
                {
                    result.Result = ActionResult.ACTION_SUCCESSFUL;
                    result.Message = MessageAlert.SAVE_DEPOSIT_SUCCESS;
                }
                else
                {
                    result.Result = ActionResult.ACTION_FAILED;
                    result.Message = MessageAlert.SAVE_DEPOSIT_FAILED;
                    result.ErrorMessage = outResultMessage;
                }
            }
            catch (Exception ex)
            {
                result.Result = ActionResult.EXCEPTION_ENCOUNTERED;
                result.Message = ex.Message;
            }
            
            return result; 
        }

        public AddRecordResult SaveBagReplacement(BagReplacement bagReplacement)
        {
            AddRecordResult result = new AddRecordResult();

            try
            {
                dao.SaveBagReplacement(bagReplacement, out int outResult);

                if (outResult == Output.SUCCESSFUL)
                {
                    result.Result = ActionResult.ACTION_SUCCESSFUL;
                    result.Message = MessageAlert.SAVE_BAG_REPLACEMENT_SUCCESS;
                }
                else
                {
                    result.Result = ActionResult.ACTION_FAILED;
                    result.Message = MessageAlert.SAVE_BAG_REPLACEMENT_FAILED;
                }
            }
            catch (Exception ex)
            {
                result.Result = ActionResult.EXCEPTION_ENCOUNTERED;
                result.Message = ex.Message;
            }

            return result;
        }
    }
}
