﻿using Service.ClassLibrary.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Service.ClassLibrary.BusinessLogicLayer;
using Service.ClassLibrary.Integrations;
using Service.ClassLibrary.Models.Response;
using System.Globalization;
using System.IO;
using Service.ClassLibrary.Models.Domain;
using Service.ClassLibrary.Models.Result;
using System.Configuration;
using Serilog;

namespace Service.Remittance
{
    public class ServiceController
    {
        //System.Threading.Timer m_timer;
        //System.Threading.Timer m_timer2;
        //System.Threading.Timer m_timerday;

        FileSystemWatcher watcher;
        FileSystemWatcher watcher2;
        AppConfig config;

        DataTable dt = new DataTable();

        public ServiceController()
        {
            Log.Logger = new LoggerConfiguration()
           .MinimumLevel.Debug()
           .WriteTo.Console()
           .WriteTo.File(string.Concat(ConfigurationManager.AppSettings["LogsFolder"].ToString(),"Log-.txt"), rollingInterval: RollingInterval.Day)
           .CreateLogger();

            watcher = new FileSystemWatcher();
            config = AppConfig.GetSingleton();

            //  m_timer = new Timer(new TimerCallback(OnTimerTick), null, 0, 5000 * 60);

        }

        public void Start()
        {
            Log.Information("");
            Log.Information("======================================================");
            Log.Information("Windows Service Started");

            this.CreateFolders();

            watcher = new FileSystemWatcher();
            watcher.Created += new FileSystemEventHandler(watcher_Created);
            watcher.Path = Path.GetDirectoryName(ConfigurationManager.AppSettings["DepositImportPath"].ToString());
            watcher.Filter = Path.GetFileName(ConfigurationManager.AppSettings["FileFilter"].ToString());
            watcher.EnableRaisingEvents = true;
            watcher.IncludeSubdirectories = false;

            watcher2 = new FileSystemWatcher();
            watcher2.Created += new FileSystemEventHandler(watcher_Created);
            watcher2.Path = Path.GetDirectoryName(ConfigurationManager.AppSettings["BagReplacmentImportPath"].ToString());
            watcher2.Filter = Path.GetFileName(ConfigurationManager.AppSettings["FileFilter"].ToString());
            watcher2.EnableRaisingEvents = true;
            watcher2.IncludeSubdirectories = false;

            ServiceController.ResumeOnError(() => { throw new NotSupportedException(); });
            ServiceController.ResumeOnError(() => { Console.WriteLine(); });
            ServiceController.ResumeOnError(() => { Console.ReadLine(); });
        }

        public static Exception ResumeOnError(Action action)
        {
            try
            {
                action();
                return null;
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        public void Stop()
        {
            watcher.EnableRaisingEvents = false;
            Log.Information("Windows Service Stopped");
            Log.Information("======================================================");
        }

        //private void OnTimerTick(object state)
        //{

        //}


        private void watcher_Created(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType == WatcherChangeTypes.Created)
            {
                string fileName = "";
                string fileName2 = "";
                fileName = e.FullPath;
                fileName2 = Path.GetFileName(fileName);

                int i = 0;
                while (!IsFileReady(e.FullPath))
                {
                    if (!File.Exists(e.FullPath))
                        return;
                    Thread.Sleep(100);
                    i = i + 1;
                    if (i > 10)
                        break;
                }

                try
                {
                    if (fileName2.Substring(0, 7) == "DEPOSIT")
                    {
                        ProcessFile(fileName, fileName2);
                    }
                    else
                    {
                        ProcessBagRelacement(fileName, fileName2);
                    }

                }
                catch (System.Exception ex)
                {
                    Log.Error("Exception - Watcher_Created:" + ex.InnerException.ToString());    
                }
                finally
                {

                }
            }
        }

        private void watcher_Changed(object sender, FileSystemEventArgs e)
        {

        }

        private void CreateFolders()
        {
            string depositFolder = ConfigurationManager.AppSettings["DepositImportPath"].ToString();
            string bagReplacementFolder = ConfigurationManager.AppSettings["BagReplacmentImportPath"].ToString();
            string depositArchiveFolder = ConfigurationManager.AppSettings["DepositArchivePath"].ToString();
            string bagReplacementArchiveFolder = ConfigurationManager.AppSettings["BagReplacementArchivePath"].ToString();
            string logsFolder = ConfigurationManager.AppSettings["LogsFolder"].ToString();
            string depositErrorFolder = ConfigurationManager.AppSettings["DepositErrorPath"].ToString();
            string bagReplacementErrorFolder = ConfigurationManager.AppSettings["BagReplacementErrorPath"].ToString();

            if (!Directory.Exists(depositFolder))
                Directory.CreateDirectory(depositFolder);

            if (!Directory.Exists(bagReplacementFolder))
                Directory.CreateDirectory(bagReplacementFolder);

            if (!Directory.Exists(depositArchiveFolder))
                Directory.CreateDirectory(depositArchiveFolder);

            if (!Directory.Exists(bagReplacementArchiveFolder))
                Directory.CreateDirectory(bagReplacementArchiveFolder);

            if (!Directory.Exists(depositErrorFolder))
                Directory.CreateDirectory(depositErrorFolder);

            if (!Directory.Exists(bagReplacementErrorFolder))
                Directory.CreateDirectory(bagReplacementErrorFolder);

            if (!Directory.Exists(logsFolder))
                Directory.CreateDirectory(logsFolder);
        }

        bool IsFileReady(string filename)
        {
            FileInfo fi = new FileInfo(filename);
            FileStream fs = null;
            try
            {
                fs = fi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                return true;
            }
            catch (IOException)
            {
                return false;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        string xmlString;

        private void ProcessFile(string FileName, string FileName2)
        {
            string[] lines = System.IO.File.ReadAllLines(FileName);

            DepositData header = new DepositData();

            string currency = string.Empty;
            string[] strValue;
            int qty = 0;
            try
            {
                foreach (string line in lines)
                {
                    if (line.Substring(0, 3) == "HKD" || line.Substring(0, 3) == "MOP")
                    {
                        currency = line.Substring(0, 3);
                    }
                    else
                    { 
                        strValue = line.Split(':');
                        if (strValue[0].ToString() == "DEPOSIT_ID")
                        {
                            header.DepositID = line.Substring(11);
                        }
                        else if (strValue[0].ToString() == "TIME")
                        {
                            header.DepositDateTime = line.Substring(5);
                        }
                        else if (strValue[0].ToString() == "USER")
                        {
                            header.User = line.Substring(5);
                        }
                        else if (strValue[0].ToString() == "MACHINE_ID")
                        {
                            header.MachineID = line.Substring(11);
                        }
                        else if (strValue[0].ToString() == "BOX")
                        {
                            header.Box = line.Substring(4);
                        }
                        else if (strValue[0].ToString() == "01000")
                        {
                            qty = int.Parse(line.Substring(6));
                            AddDepositDetails(currency, 1000, qty);
                        }
                        else if (strValue[0].ToString() == "00500")
                        {
                            qty = int.Parse(line.Substring(6));
                            AddDepositDetails(currency, 500, qty);
                        }
                        else if (strValue[0].ToString() == "00100")
                        {
                            qty = int.Parse(line.Substring(6));
                            AddDepositDetails(currency, 100, qty);
                        }
                        else if (strValue[0].ToString() == "00050")
                        {
                            qty = int.Parse(line.Substring(6));
                            AddDepositDetails(currency, 50, qty);
                        }
                        else if (strValue[0].ToString() == "00020")
                        {
                            qty = int.Parse(line.Substring(6));
                            AddDepositDetails(currency, 20, qty);
                        }
                        else if (strValue[0].ToString() == "00010")
                        {
                            qty = int.Parse(line.Substring(6));
                            AddDepositDetails(currency, 10, qty);
                        }
                    }
     
                }

                xmlString = string.Concat("<Deposit>", xmlString, "</Deposit>");

                header.DepositDetails = xmlString;

                this.SaveDepositData(header, FileName, FileName2);
            }
            catch (Exception ex)
            {
                Log.Error(string.Concat("Exception - Process Deposit File: ", FileName, " - ", ex.Message.ToString(), " File moved to Error folder"));
                System.IO.File.Copy(FileName, string.Concat(ConfigurationManager.AppSettings["DepositErrorPath"].ToString(), "\\", FileName2), true);
                System.IO.File.Delete(FileName);
            }
            
        }
    
        private void ProcessBagRelacement(string FileName, string FileName2)
        {
            string[] lines = System.IO.File.ReadAllLines(FileName);

            BagReplacement bag = new BagReplacement();

            try
            {
                foreach (string line in lines)
                {
                    if (line.Substring(0, 4) == "TIME")
                    {
                        bag.Time = line.Substring(5, 14);
                    }
                    else if (line.Substring(0, 4) == "USER")
                    {
                        bag.User = line.Substring(5, 10);
                    }
                    else if (line.Substring(0, 7) == "OLD_BOX")
                    {
                        bag.OldBox = line.Substring(8, 20);
                    }
                    else if (line.Substring(0, 7) == "NEW_BOX")
                    {
                        bag.NewBox = line.Substring(8, 20);
                    }
                    else if (line.Substring(0, 8) == "END_TIME")
                    {
                        bag.EndTime = line.Substring(9, 14);
                    }
                    else if (line.Substring(0, 10) == "START_TIME")
                    {
                        bag.StartTime = line.Substring(11, 14);
                    }
                    else if (line.Substring(0, 10) == "MACHINE_ID")
                    {
                        bag.MachineID = line.Substring(11, 6);
                    }
                }

                this.SaveBagReplacment(bag, FileName, FileName2);
            }
            catch (Exception ex)
            {
                Log.Error(string.Concat("Exception - Process Bag Replacement File: ", FileName, " - ", ex.Message.ToString(), " File moved to Error folder"));
                System.IO.File.Copy(FileName, string.Concat(ConfigurationManager.AppSettings["BagReplacementErrorPath"].ToString(), "\\", FileName2), true);
                System.IO.File.Delete(FileName);
            }

        }
        private void AddDepositDetails(string Currency, int Denomination, int Qty)
        {
            //DepositDetails details = new DepositDetails();

            //details.Currency = Currency;
            //details.Denomination = Denomination.ToString();
            //details.Qty = Qty.ToString();

            ////Header.Deposit.DepositDetails.Add(new DepositDetails(Currency, Denomination, Qty));

            //deposit.DepositDetails.Add(details);

            xmlString = string.Concat(xmlString, "<DepositDetails Currency =", (char)34, Currency, (char)34, " Denomination =", (char)34, Denomination, (char)34, " Qty =", (char)34, Qty, (char)34, "/>");

        }

        private void SaveBagReplacment(BagReplacement bagReplacement, string FileName, string FileName2)
        {
            RCDTransactionBLL bll = new RCDTransactionBLL();
            AddRecordResult result = bll.SaveBagReplacement(bagReplacement);

            if (result.Result == ActionResult.ACTION_SUCCESSFUL)
            {
                Log.Information(string.Concat(result.Message, "- Filename:", FileName));
                System.IO.File.Copy(FileName, string.Concat(ConfigurationManager.AppSettings["BagReplacementArchivePath"].ToString(), "\\", FileName2), true);
                System.IO.File.Delete(FileName);
            }
            else if (result.Result == ActionResult.ACTION_FAILED)
            {
                Log.Warning(string.Concat(result.Message, "- Filename:", FileName, " File moved to Error folder"));
                System.IO.File.Copy(FileName, string.Concat(ConfigurationManager.AppSettings["BagReplacementErrorPath"].ToString(), "\\", FileName2), true);
                System.IO.File.Delete(FileName);
            }
            else if (result.Result == ActionResult.EXCEPTION_ENCOUNTERED)
            {
                Log.Error(string.Concat("Exception encountered - ", result.Message, "- Filename:", FileName, " File moved to Error folder"));
                System.IO.File.Copy(FileName, string.Concat(ConfigurationManager.AppSettings["BagReplacementErrorPath"].ToString(), "\\", FileName2), true);
                System.IO.File.Delete(FileName);
            }
        }

        private void SaveDepositData(DepositData depositData, string FileName, string FileName2)
        {
            RCDTransactionBLL bll = new RCDTransactionBLL();
            AddRecordResult result = bll.SaveDepositData(depositData);
            
            if(result.Result == ActionResult.ACTION_SUCCESSFUL)
            {
                Log.Information(string.Concat(result.Message, "- Filename:", FileName));
                System.IO.File.Copy(FileName, string.Concat(ConfigurationManager.AppSettings["DepositArchivePath"].ToString(), "\\", FileName2), true);
                System.IO.File.Delete(FileName);
            }
            else if(result.Result == ActionResult.ACTION_FAILED)
            {
                Log.Warning(string.Concat(result.ErrorMessage, "- Filename:", FileName, " File moved to Error folder"));
                Log.Warning(string.Concat(result.Message, "- Filename:", FileName));
                System.IO.File.Copy(FileName, string.Concat(ConfigurationManager.AppSettings["DepositErrorPath"].ToString(), "\\", FileName2), true);
                System.IO.File.Delete(FileName);
            }
            else if(result.Result == ActionResult.EXCEPTION_ENCOUNTERED)
            {
                Log.Error(string.Concat("Exception encountered - ", result.Message, "- Filename:", FileName, " File moved to Error folder"));
                System.IO.File.Copy(FileName, string.Concat(ConfigurationManager.AppSettings["DepositErrorPath"].ToString(), "\\", FileName2), true);
                System.IO.File.Delete(FileName);
            }

        }
    }
}
