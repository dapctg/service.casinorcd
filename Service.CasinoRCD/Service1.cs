﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Service.Remittance
{
    public partial class Service1 : ServiceBase
    {
        ServiceController sc = new ServiceController();
        public Service1()
        {
            InitializeComponent();
            sc.Start();
        }

        protected override void OnStart(string[] args)
        {
            sc.Start();
        }

        protected override void OnStop()
        {
            sc.Stop();
        }
    }
}
